package utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import model.CentroExposicoes;
import model.FAE;
import model.Organizador;
import model.Utilizador;

/**
 * 
 * @author Catarina
 */

public class Descodificador {

    public static int lerFicheiro_Utilizadores(CentroExposicoes ce) throws FileNotFoundException {
        int count = 0;
        Scanner file_u = new Scanner(new File("Ficheiro_Utilizadores.txt"));

        while (file_u.hasNextLine()) {

            String line = file_u.nextLine();
            String[] tempVec;

            if (!line.isEmpty()) {
                tempVec = line.split(" - ");

                if (tempVec[0].equalsIgnoreCase("[UtilComRegistoConfirmado]")) {
                    count++;
                    System.out.println("estou aqui");
                    Utilizador u = new Utilizador(tempVec[1], tempVec[2], tempVec[3], tempVec[4], true);
                    ce.getRegistoUtilizadores().registaUtilizador(u);

                    System.out.println("Foi carregado para memória o utilizador com registo confirmado:\n" + u.toString());

                } else if (tempVec[0].equalsIgnoreCase("[UtilSemRegistoConfirmado]")) {
                    count++;
                    Utilizador u = new Utilizador(tempVec[1], tempVec[2], tempVec[3], tempVec[4], false);

                    ce.getRegistoUtilizadores().registaUtilizador(u);
                    System.out.println("Foi carregado para memória o utilizador sem registo confirmado:\n" + u.toString());

                } else if (tempVec[0].equalsIgnoreCase("[FAE]")) {
                    count++;
                    Utilizador u = new Utilizador(tempVec[1], tempVec[2], tempVec[3], tempVec[4], true);

                    ce.getRegistoUtilizadores().registaUtilizador(u);
                    FAE fae = new FAE(u);
                    System.out.println("Foi carregado para memória o fae :\n" + fae.toString());

                } else if (tempVec[0].equalsIgnoreCase("[Organizador]")) {
                    count++;
                    Utilizador u = new Utilizador(tempVec[1], tempVec[2], tempVec[3], tempVec[4], true);

                    ce.getRegistoUtilizadores().registaUtilizador(u);
                    Organizador org = new Organizador(u);
                    System.out.println("Foi carregado para memória o organizador :\n" + org.toString());

                }

            }
            System.out.println("\t\t====== Fim da leitura de Ficheiro de Utilizadores ======\n");

        }
        file_u.close();

        return count;
    }
}

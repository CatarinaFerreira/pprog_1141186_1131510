/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import controller.RegistarUtilizadorController;
import java.util.Scanner;
import model.CentroExposicoes;
import model.Utilizador;

/**
 *
 * @author Catarina
 */
public class RegistarUtilizadorUI {
    
    private final RegistarUtilizadorController controller;
    public final Utilizador m_oUu;
    Scanner ler;
    
    public RegistarUtilizadorUI(CentroExposicoes ce, Utilizador u) {
        controller = new RegistarUtilizadorController(ce);
        m_oUu=u;
        ler = new Scanner(System.in);
    }
    
    public void run() {
        System.out.println("*** Registar Utilizador ***: ");

        do {
            controller.novoUtilizador();
            introduzDados();
            
            System.out.print("Continuar (s/n)?");
        }while ("s".equalsIgnoreCase(ler.nextLine()));      
    }
    
    public void introduzDados() {

        String nome, email, username, password;
        System.out.println("*** Introduza os dados pedidos ***: ");
        System.out.print("Nome    :"); nome = ler.nextLine();
        System.out.print("Email   :"); email = ler.nextLine();
        System.out.print("Username:"); username = ler.nextLine();
        System.out.print("Password:"); password = ler.nextLine();

        if(controller.setUtilizador(nome, email, username, password)) {
            System.out.println(controller.getUtilizadorString());
            System.out.print("Confirma (s/n)?");
            
            if("s".equalsIgnoreCase(ler.nextLine())) {
                if( controller.registaUtilizador())
                    System.out.println("Operação efetuada com sucesso");
                else
                    System.out.println("ERRO: FALHOU VALIDAÇÃO GLOBAL!");
            }
        }
        else
            System.out.println("ERRO: FALHOU VALIDAÇÃO LOCAL!");
    } 
}

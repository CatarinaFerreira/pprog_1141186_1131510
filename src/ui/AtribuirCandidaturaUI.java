/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import controller.AtribuirCandidaturaController;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.BevelBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import model.Atribuicao;
import model.CentroExposicoes;
import model.Exposicao;
import model.MecanismoAtribuicao;

/**
 *
 * @author Catarina
 */
public class AtribuirCandidaturaUI extends JPanel implements TabChangeListener{

    private CentroExposicoes ce;
    
    private AtribuirCandidaturaController acc;
    
    private DefaultListModel<Exposicao> expModel;
    
    private JList<Exposicao> expList;
    
    private JList<MecanismoAtribuicao> mecList;
    
    private DefaultListModel<MecanismoAtribuicao> mecModel;
    
    private JList<Atribuicao> atrList;
    
    private DefaultListModel<Atribuicao> atrModel;
    
    /**
     * Certifica-se que não podemos criar uma UI inválida só por ser do tipo JPanel
     */
    
    private AtribuirCandidaturaUI() {
        
    }
    
    public AtribuirCandidaturaUI(CentroExposicoes m_ce) {
        super();
        this.ce=m_ce;
        this.acc=new AtribuirCandidaturaController(ce);
    }
    
    private void displayError(String message)
    {
        JOptionPane.showMessageDialog(this,message,"ERRO",JOptionPane.ERROR_MESSAGE);
    }

    private void buildUI() {
        setLayout(new GridLayout(4, 1));
        addExpositionList();
        addMechanismUI();
        addAttributionUI();
        addRegisterUI();
    }
    
    private void addRegisterUI() {
        JPanel mainPanel = new JPanel();
        JButton registerButton = new JButton();
        registerButton.setText("Registar Atribuições");
        
        registerButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (acc.registerAtribuicoes()) {
                    JOptionPane.showMessageDialog(null, "Registo efetuado com sucesso!");
                    buildUI();
                }
                else {
                    displayError("Erro ao registar as atribuições.");
                }
            }
        });
        mainPanel.add(registerButton);
        add(mainPanel);
    }
    
    private void addAttributionUI() {
        atrList = new JList();
        atrModel = new DefaultListModel();
        atrList.setModel(atrModel);
        JScrollPane pane = new JScrollPane(atrList);
        
        pane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED, Color.lightGray, Color.lightGray), "Atribuições"));
        add(pane);
    }
    
    private void addMechanismUI() {
        JPanel mainPanel = new JPanel();
        mecList = new JList();
        mecModel = new DefaultListModel();
        mecList.setModel(mecModel);
        JScrollPane pane = new JScrollPane(mecList);
        
        pane.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createLineBorder(Color.GRAY,2,true), "Mecanismos de Atribuição"));
        add(pane);
        mecList.addListSelectionListener(new ListSelectionListener()
        {

            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    atrModel.clear();
                    acc.setMecanismo(mecList.getSelectedValue());
                    List<Atribuicao> list = acc.getListaAtribuicoes();
                    if (!list.isEmpty()) {
                        for (Atribuicao element:list)
                        {
                            atrModel.addElement(element);
                        }
                    }
                    else {
                        displayError("Não existem atribuições para o mecanismo e exposição selecionados.");
                    }
                }
            }
        });
        //add(mainPanel);
    }
    
    private void addExpositionList() {
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new GridLayout(2,1));
        
        JPanel selectPanel = new JPanel();
        expModel = new DefaultListModel();
        expList = new JList();
        expList.setModel(expModel);
        expList.setMinimumSize(new Dimension(256,256));
        expList.setPreferredSize(new Dimension(256,256));
        JScrollPane pane = new JScrollPane(expList);
        
        
        pane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.GRAY,2,true), "Exposições"));
        JButton selectButton = new JButton("Selecionar Exposição");
        selectButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (expList.getSelectedValue()!=null) {
                    acc.setExposicao(expList.getSelectedValue());
                    List<MecanismoAtribuicao> list = acc.getListaMecanismos();
                    if (!list.isEmpty()) {
                        for (MecanismoAtribuicao element:list)
                        {
                            mecModel.addElement(element);
                        }
                    }
                    else
                    {
                        displayError("Não existem mecanismos de atribuição.");
                    }
                }
            }
        });
        selectPanel.add(selectButton);
        
        mainPanel.add(pane);
        mainPanel.add(selectPanel);
        add(mainPanel);
    }

    @Override
    public void onTabChanged() {
        removeAll();
        buildUI();
        EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
                List<Exposicao> list = acc.getListaExposicoes(ce.getLoggedInUser());
                if (!list.isEmpty()) {
                    for (Exposicao element:list) {
                        expModel.addElement(element);
                    }
                }
                else {
                    displayError("Não tem nenhuma exposição.");
                }
            }
        });
    }
}

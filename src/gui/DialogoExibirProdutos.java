/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import controller.DecidirCandidaturasController;
import model.Produto;

/**
 *
 * @author Catarina
 */
public class DialogoExibirProdutos extends JDialog {
    
    private DecidirCandidaturasController controller;
    private List<Produto> listaProduto;
    private JTextArea txtlistaProduto;

    public DialogoExibirProdutos(JFrame framePai, DecidirCandidaturasController controller) {
        super(framePai, "Produtos");
        this.controller=controller;
        
        criarComponentes();

        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setResizable(false);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private void criarComponentes() {
        JScrollPane pLista=criarPainelLista();
        JPanel pBotao = criarPainelBotao();
        
        add(pLista,BorderLayout.CENTER);
        add(pBotao,BorderLayout.SOUTH);
    }

    private JScrollPane criarPainelLista() {
        listaProduto = (List<Produto>) controller.getProduto();

        txtlistaProduto = new JTextArea(listaProduto.toString());
        txtlistaProduto.setEditable(false);

        JScrollPane scrollPane = new JScrollPane(txtlistaProduto);

        final int MARGEM_SUPERIOR = 20, MARGEM_INFERIOR = 20;
        final int MARGEM_ESQUERDA = 20, MARGEM_DIREITA = 20;
        scrollPane.setBorder(BorderFactory.createEmptyBorder(MARGEM_SUPERIOR,
                MARGEM_ESQUERDA,
                MARGEM_INFERIOR,
                MARGEM_DIREITA));

        return scrollPane;
    }
    private JPanel criarPainelBotao() {
        JPanel p = new JPanel();
        p.add(criarBotaoGuardar());
        return p;
    }

    private JButton criarBotaoGuardar() {
        JButton btn = new JButton("Guardar");

        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        return btn;
    } 
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import controller.RegistarCandidaturaController;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import model.CentroExposicoes;
import model.RegistoCandidaturas;

/**
 *
 * @author Catarina
 */

public class DialogoCandidatura extends JDialog {
    
    private JTextField txtNome;
    private JTextField txtMorada;
    private JTextField txtTelemovel;
    private JTextField txtArea;
    private JTextField txtConvites;
    private JTextField txtProduto;
    private final JFrame frame;
    private final JTabbedPane tabPane;
    
    private RegistarCandidaturaController controller;
    private PainelListaCandidaturas pListaCandidaturas;
    
    /**
     * Constrói a Janela que irá conter espaços indicados para o preenchimento dos dados necessários para o registo de
     * uma candidatura.
     * @param frame frame.
     * @param listaCandidaturas lista candidaturas.
     * @param tabPane tab pane.
     * @param pListaCandidaturas painel lista lista candidaturas.
     * @param ce centro exposiçoes.
     */
   
    public DialogoCandidatura( JFrame frame, 
                                RegistoCandidaturas listaCandidaturas, 
                                JTabbedPane tabPane, 
                                PainelListaCandidaturas pListaCandidaturas,CentroExposicoes ce) {

        super(frame, "Registar Candidatura", true);

        controller = new RegistarCandidaturaController(ce);
        final int NUMERO_LINHAS = 3, NUMERO_COLUNAS = 1;
        setLayout(new GridLayout(NUMERO_LINHAS,NUMERO_COLUNAS));
    
        this.frame = frame;
        this.tabPane = tabPane;
        this.pListaCandidaturas = pListaCandidaturas;
        
        criarComponentes();
        
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        
        pack();
        setResizable(false);
        setLocationRelativeTo(frame);
        setVisible(true);
    }
    
    /**
     * Cria e adiciona componentes à Janela.
     */
    
    private void criarComponentes(){
        JPanel p1 = criarPainelNome();
        JPanel p2 = criarPainelMorada();
        JPanel p3 = criarPainelTelemovel();
        JPanel p4 = criarPainelArea();
        JPanel p5 = criarPainelConvites();
        JPanel p6 = criarPainelProduto();
        JPanel p7 = criarPainelBotoes();

        add(p1);
        add(p2);
        add(p3);
        add(p4);
        add(p5);
        add(p6);
        add(p7);
    }
    
    /**
     * Constrói o painel com a label necessária para a introdução do nome comercial do expositor.
     * @return 
     */
    
    private JPanel criarPainelNome() {
        JLabel lbl = new JLabel("Nome Comercial:", JLabel.RIGHT);

        final int CAMPO_LARGURA = 20;
        txtNome = new JTextField(CAMPO_LARGURA);
        txtNome.requestFocus();

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtNome);

        return p;
    }
    
    /**
     * Constrói o painel com a label necessária para a introdução da morada do expositor.
     * @return 
     */
    
    private JPanel criarPainelMorada() {
        JLabel lbl = new JLabel ("Morada:", JLabel.RIGHT);
        
        final int CAMPO_LARGURA = 20;
        txtMorada = new JTextField(CAMPO_LARGURA);
        txtMorada.requestFocus();
    
        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtMorada);

        return p;
    }
    
    /**
     * Constrói o painel com a label necessária para a introdução do telemóvel do expositor.
     * @return 
     */
    
    private JPanel criarPainelTelemovel() {
        JLabel lbl = new JLabel ("Telemóvel:", JLabel.RIGHT);
        
        final int CAMPO_LARGURA = 20;
        txtTelemovel = new JTextField(CAMPO_LARGURA);
        txtTelemovel.requestFocus();
    
        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtTelemovel);

        return p;    
    }
    
    /**
     * Constrói o painel com a label necessária para a introdução da área pretendida pelo expositor.
     * @return 
     */
    
    private JPanel criarPainelArea() {
        JLabel lbl = new JLabel ("Área de exposição pretendida:", JLabel.RIGHT);
        
        final int CAMPO_LARGURA = 20;
        txtArea = new JTextField(CAMPO_LARGURA);
        txtArea.requestFocus();
    
        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtArea);

        return p;    
    }
    
    /**
     * Constrói o painel com a label necessária para a introdução da quantidade de convites pretendida pelo 
     * expositor.
     * @return 
     */
    
    private JPanel criarPainelConvites() {
        JLabel lbl = new JLabel ("Número de convites a adquirir:", JLabel.RIGHT);
        
        final int CAMPO_LARGURA = 20;
        txtConvites = new JTextField(CAMPO_LARGURA);
        txtConvites.requestFocus();
    
        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtConvites);

        return p;    
    } 
    
    /**
     * Constrói o painel com a label necessária para a introdução do produto que o expositor pretende expor.
     * @return 
     */
    
    private JPanel criarPainelProduto() {
        JLabel lbl = new JLabel ("Produto a expor:", JLabel.RIGHT);
        
        final int CAMPO_LARGURA = 20;
        txtProduto = new JTextField(CAMPO_LARGURA);
        txtProduto.requestFocus();
    
        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtProduto);

        return p;    
    }   
    
    /**
     * Constrói o painel que irá conter 2 botões que irão permitir guardar a candidatura ou cancelar o seu 
     * registo.
     * @return 
     */
    
    private JPanel criarPainelBotoes() {
        JButton btnGuardar = criarBotaoGuardar();
        getRootPane().setDefaultButton(btnGuardar);

        JButton btnCancelar = criarBotaoCancelar();

        JPanel p = new JPanel();
        final int MARGEM_SUPERIOR = 0, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA,
                MARGEM_INFERIOR, MARGEM_DIREITA));
        p.add(btnGuardar);
        p.add(btnCancelar);

        return p;
    }
    
    /**
     * Constrói o botão Guardar que irá registar os dados inseridos referentes à candidatura e adicionará os mesmos
     * ao separador correspondente.
     * @return 
     */
    
    private JButton criarBotaoGuardar() {
        JButton btn = new JButton("Guardar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    controller.novaCandidatura();
                    String m_sNomeComercial = txtNome.getText();
                    String m_sMorada = txtMorada.getText();
                    int m_iTelemovel = Integer.parseInt(txtTelemovel.getText());
                    Double m_dAreaExposicao = Double.parseDouble(txtArea.getText());
                    int m_iQuantidadeConvites = Integer.parseInt(txtConvites.getText());
                    String m_pNome = txtProduto.getText();
                    
                    controller.setDados(m_sNomeComercial, m_sMorada, m_iTelemovel, m_dAreaExposicao, m_iQuantidadeConvites, m_pNome);
                    boolean candidaturaAdicionada = controller.registaCandidatura();
                    
                    if (candidaturaAdicionada) {
                        JOptionPane.showMessageDialog(
                                frame,
                                "Candidatura adicionada às candidaturas registadas.",
                                "Nova Candidatura",
                                JOptionPane.INFORMATION_MESSAGE);
                        pListaCandidaturas.onTabChanged();
                        dispose();
                    } else {
                        JOptionPane.showMessageDialog(
                                frame,
                                "Candidatura já incluída nas candidaturas registadas ou é invalida!",
                                "Nova Candidatura",
                                JOptionPane.ERROR_MESSAGE);
                    }

                    final int TAB_LISTA_CANDIDATURAS = 1;                    
                    tabPane.setSelectedIndex(TAB_LISTA_CANDIDATURAS);   
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(
                            frame,
                            "Tem que introduzir um número inteiro com 9 digitos.",
                            "Novo Telemovel",
                            JOptionPane.WARNING_MESSAGE);
                    txtTelemovel.requestFocusInWindow();
                } catch (IllegalArgumentException ex) {
                    JOptionPane.showMessageDialog(
                            frame,
                            ex.getMessage(),
                            "Nova Candidatura",
                            JOptionPane.WARNING_MESSAGE);
                    txtNome.requestFocusInWindow();
                }
            }
        });
        return btn;
    }
    
    /**
     * Constrói o botão Cancelar que cancela automaticamente o registo da candidatura.
     * @return 
     */
    
    private JButton criarBotaoCancelar() {
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        return btn;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import controller.DecidirCandidaturasController;
import model.Candidatura;
import model.CentroExposicoes;
import model.Exposicao;
import model.FAE;

/**
 *
 * @author Catarina
 */
public class DialogoEscolherCandidatura extends JDialog {

    private final JFrame framePai;
    private Exposicao expo;
    private FAE fae;
    private CentroExposicoes centroExposicoes;
    private DecidirCandidaturasController deci_controller;
    private JComboBox comboCandidaturas;
    private static final Dimension DIMENSION = new Dimension(1000, 300);

    public DialogoEscolherCandidatura(JFrame framePai, CentroExposicoes centroExposicoes, FAE fae, Exposicao expo) {

        super(framePai, "Escolha Candidatura", true);
        this.framePai = framePai;
        this.centroExposicoes = centroExposicoes;
        this.fae = fae;
        this.expo = expo;
        this.deci_controller = new DecidirCandidaturasController(centroExposicoes, expo);

        setLayout(new BorderLayout());
        setPreferredSize(DIMENSION);

        criarComponentes();
        pack();
        setResizable(false);
        setLocationRelativeTo(framePai);
        setVisible(true);
    }

    public void criarComponentes() {
        JPanel p1 = criarPainelCandidatura();
        JPanel p2 = criarPainelBotoes();
        add(p1, BorderLayout.NORTH);
        add(p2, BorderLayout.SOUTH);
    }

    private JPanel criarPainelCandidatura() {
        JPanel p = new JPanel();
        p.add(criarComboBoxCandidaturas());
        return p;
    }

    private JPanel criarPainelBotoes() {
        JPanel p = new JPanel();
        p.add(criarBotaoGuardar());
        p.add(criarBotaoCancelar());
        return p;
    }

    private JComboBox criarComboBoxCandidaturas() {
        comboCandidaturas = new JComboBox(deci_controller.getListaCandidaturasFAE(fae).toArray());

        comboCandidaturas.setSelectedIndex(-1);
        return comboCandidaturas;
    }

    private JButton criarBotaoGuardar() {
        JButton btn = new JButton("Guardar");
        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                Candidatura c = (Candidatura) comboCandidaturas.getSelectedItem();

                deci_controller.setCandidatura(c);
                dispose();
                new DialogoDecidirCandidatura(framePai, deci_controller);
            }
        });
        return btn;
    }

    private JButton criarBotaoCancelar() {
        JButton btn = new JButton("Cancelar");

        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        return btn;
    }
}

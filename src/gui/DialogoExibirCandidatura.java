/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import controller.DecidirCandidaturasController;
import model.CentroExposicoes;
import model.Exposicao;

/**
 *
 * @author Catarina
 */
public class DialogoExibirCandidatura extends JDialog {
    
    private final JFrame framePai;
    private DecidirCandidaturasController controller;
    
    private static final int NUMERO_FILAS = 9;
    private static final int NUMERO_COLUNAS = 1;
    private JTextField txtEmpresa;
    private JTextField txtMorada;
    private JTextField txtTelemovel;
    private JTextField txtAreaPretendida;
    private JTextField txtQuantidadeConvites;
    private JComboBox comboDemonstracoes;
    private static final Dimension TAMANHO = new Dimension(200, 20);

    public DialogoExibirCandidatura(JFrame framePai,CentroExposicoes ce,Exposicao ep) {
        super(framePai, "Criar Candidatura");
        this.framePai = framePai;
        
        this.controller = new DecidirCandidaturasController(ce, ep);
        criarComponentes();
        setLayout(new GridLayout(NUMERO_FILAS, NUMERO_COLUNAS));
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setResizable(false);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private void criarComponentes() {

        add(criarPainelNomeComercial());
        add(criarPainelMorada());
        add(criarPainelTelemovel());
        add(criarPainelAreaPretendida());
        add(criarPainelQuantidadeConvites());
        add(criarPainelProdutos());
        add(criarPainelBotoes());
    }

    private JPanel criarPainelNomeComercial() {
        JLabel lbl = new JLabel("Nome comercial:", JLabel.LEFT);
        lbl.setPreferredSize(TAMANHO);

        final int CAMPO_LARGURA = 20;
        txtEmpresa = new JTextField(CAMPO_LARGURA);
        txtEmpresa.setText(controller.getNomeComercial());
        txtEmpresa.setEditable(false);

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA, MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtEmpresa);

        return p;
    }

    private JPanel criarPainelMorada() {
        JLabel lbl = new JLabel("Morada:", JLabel.LEFT);
        lbl.setPreferredSize(TAMANHO);

        final int CAMPO_LARGURA = 20;
        txtMorada = new JTextField(CAMPO_LARGURA);
        txtMorada.setText(controller.getMorada());
        txtMorada.setEditable(false);

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA, MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtMorada);

        return p;
    }

    private JPanel criarPainelTelemovel() {
        JLabel lbl = new JLabel("Telemóvel:", JLabel.LEFT);
        lbl.setPreferredSize(TAMANHO);

        final int CAMPO_LARGURA = 20;
        txtTelemovel = new JTextField(CAMPO_LARGURA);
        txtTelemovel.setText(Integer.toString(controller.getTelemovel()));
        txtTelemovel.setEditable(false);
        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA, MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtTelemovel);

        return p;
    }

    private JPanel criarPainelAreaPretendida() {
        JLabel lbl = new JLabel("Área de exposição pretendida:", JLabel.LEFT);
        lbl.setPreferredSize(TAMANHO);

        final int CAMPO_LARGURA = 20;
        txtAreaPretendida = new JTextField(CAMPO_LARGURA);
        txtAreaPretendida.setText(Float.toString((float) controller.getAreaPretendida()));
        txtAreaPretendida.setEditable(false);
        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA, MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtAreaPretendida);

        return p;
    }

    private JPanel criarPainelQuantidadeConvites() {
        JLabel lbl = new JLabel("Número de convites a adquirir:", JLabel.LEFT);
        lbl.setPreferredSize(TAMANHO);

        final int CAMPO_LARGURA = 20;
        txtQuantidadeConvites = new JTextField(CAMPO_LARGURA);
        txtQuantidadeConvites.setText(Integer.toString(controller.getQuantidadeConvites()));
        txtQuantidadeConvites.setEditable(false);

        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA, MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lbl);
        p.add(txtQuantidadeConvites);

        return p;
    }

    private JPanel criarPainelProdutos() {
        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA, MARGEM_INFERIOR, MARGEM_DIREITA));

        JButton btn = criarBotaoProdutos();

        p.add(btn);
        return p;
    }

    private JPanel criarPainelBotoes() {
        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(new EmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA, MARGEM_INFERIOR, MARGEM_DIREITA));

        JButton btn1 = criarBotaoAvaliar();
        JButton btn2 = criarBotaoCancelar();
        p.add(btn1);
        p.add(btn2);
        return p;
    }

    private JButton criarBotaoProdutos() {
        JButton btn = new JButton("Produtos");

        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                new DialogoExibirProdutos(framePai, controller);
            }
        });
        return btn;
    }
    
    private JButton criarBotaoAvaliar() {
        JButton btn = new JButton("Avaliar");
        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
                new DialogoDecidirCandidatura(framePai,controller);
            }
        });
        return btn;
    }

    private JButton criarBotaoCancelar() {
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });

        return btn;
    }
    
}

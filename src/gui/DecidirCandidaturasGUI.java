/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import controller.DecidirCandidaturasController;

/**
 *
 * @author Catarina
 */
public class DecidirCandidaturasGUI extends JDialog {

    private static final String[] avaliacao = {"Aprovada", "Rejeitada"};
    private DecidirCandidaturasController controller;
    private JTextArea txtJustificao;
    private JComboBox comboBox;
    private Dimension TAMANHO = (new Dimension(400, 100));

    public DecidirCandidaturasGUI(JFrame framePai, DecidirCandidaturasController controller) {
        super(framePai, "Decisão da Candidatura");
        this.controller = controller;
        criarComponentes();
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setResizable(false);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private void criarComponentes() {
        JPanel p1 = criarPainelDecisao();
        JPanel p2 = criarPainelTexto();
        JPanel p3 = criarPainelBotoes();
        add(p1, BorderLayout.NORTH);
        add(p2, BorderLayout.CENTER);
        add(p3, BorderLayout.SOUTH);
    }

    private JPanel criarPainelDecisao() {
        JPanel p = new JPanel();
        JLabel lbl = new JLabel("Decisão");
        comboBox = new JComboBox(avaliacao);
        comboBox.setSelectedIndex(-1);
        p.add(lbl);
        p.add(comboBox);
        return p;
    }

    private JPanel criarPainelTexto() {
        JPanel p = new JPanel();

        JLabel lbl = new JLabel("Texto Justificativo");
        txtJustificao = new JTextArea();
        txtJustificao.setPreferredSize(TAMANHO);

        p.add(lbl);
        p.add(txtJustificao);

        return p;
    }

    private JPanel criarPainelBotoes() {
        JPanel p = new JPanel();
        p.add(criarBotaoSubmeter());
        p.add(criarBotaoCancelar());
        return p;
    }

    private JButton criarBotaoSubmeter() {
        JButton btn = new JButton("Submeter");

        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                int i = comboBox.getSelectedIndex();
                if (i == 0) {
                    controller.setDecisao("Aprovada");
                } else {
                    controller.setDecisao("Rejeitada");
                }
                controller.setJustificao(txtJustificao.getText());
                System.out.println(controller.toString());
                dispose();
            }
        });
        return btn;
    }

    private JButton criarBotaoCancelar() {
        JButton btn = new JButton("Cancelar");

        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }

        });
        return btn;
    }
}

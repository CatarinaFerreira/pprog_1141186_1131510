/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import controller.ListarCandidaturasController;
import java.awt.BorderLayout;
import java.awt.Color;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import model.Candidatura;
import model.CentroExposicoes;
import ui.TabChangeListener;

/**
 *
 * @author Catarina
 */

public class PainelListaCandidaturas extends JPanel implements TabChangeListener{
    
    private JList<Candidatura> list; 
    
    private DefaultListModel<Candidatura> model;
    
    private ListarCandidaturasController controller;
  
    /**
    * Constrói o painel que irá conter uma lista com todas as candidaturas.
    * 
    * @param ce centro exposicoes.
    */
    
    public PainelListaCandidaturas(CentroExposicoes ce) {
        super();
        controller = new ListarCandidaturasController(ce);
        setLayout(new BorderLayout());
        
        add(criarPainelListaCandidaturas(), BorderLayout.CENTER);
    }

    /**
     * Método que atualiza a lista de candidaturas.
     */
    
    void atualizarListaCandidaturas(){
        model.clear();
        List<Candidatura> candidaturas = controller.getListaCandidaturas();
        for (Candidatura element:candidaturas)
        {
            model.addElement(element);
        }
    }
    
    /**
     * Constrói o painel com a lista de candidaturas.
     * @return 
     */
    
    private JScrollPane criarPainelListaCandidaturas() {

        model = new DefaultListModel();
        list = new JList();
        list.setModel(model);

        JScrollPane scrollPane = new JScrollPane(list);
        
        scrollPane.setBorder(
                BorderFactory.createTitledBorder(
                        BorderFactory.createLineBorder(Color.GRAY,2,true), "Candidaturas"));
        
        return scrollPane;
    }

    /**
     * Método que permite que, quando o utilizador selecione a opção Guardar, o visor alterne automaticamente para
     * o separador Candidaturas Registadas, demonstrando os dados que o utilizador introduziu.
     */
    
    @Override
    public void onTabChanged() {
        atualizarListaCandidaturas();
    }
}

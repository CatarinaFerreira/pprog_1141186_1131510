/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import controller.ListarDecisoesController;
import java.awt.BorderLayout;
import java.awt.Color;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import model.CentroExposicoes;
import model.Decisao;
import ui.TabChangeListener;

/**
 *
 * @author Catarina
 */

public class PainelListaDecisoes extends JPanel implements TabChangeListener{
    
    private JList<Decisao> list; 
    
    private DefaultListModel<Decisao> model;
    
    private ListarDecisoesController controller;
  
    /**
    * Constrói o painel que irá conter uma lista com todas as candidaturas.
    * 
    * @param ce centro exposicoes.
    */
    
    public PainelListaDecisoes(CentroExposicoes ce) {
        super();
        controller = new ListarDecisoesController(ce);
        setLayout(new BorderLayout());
        
        add(criarPainelListaCandidaturas(), BorderLayout.CENTER);
    }

    /**
     * Método que atualiza a lista de candidaturas.
     */
    
    void atualizarListaDecisoes(){
        model.clear();
        List<Decisao> decisoes = controller.getListaDecisoes();
        for (Decisao element:decisoes)
        {
            model.addElement(element);
        }
    }
    
    /**
     * Constrói o painel com a lista de candidaturas.
     * @return 
     */
    
    private JScrollPane criarPainelListaCandidaturas() {

        model = new DefaultListModel();
        list = new JList();
        list.setModel(model);

        JScrollPane scrollPane = new JScrollPane(list);
        
        scrollPane.setBorder(
                BorderFactory.createTitledBorder(
                        BorderFactory.createLineBorder(Color.GRAY,2,true), "Decisões"));
        
        return scrollPane;
    }

    /**
     * Método que permite que, quando o utilizador selecione a opção Guardar, o visor alterne automaticamente para
     * o separador Candidaturas Registadas, demonstrando os dados que o utilizador introduziu.
     */
    
    @Override
    public void onTabChanged() {
        atualizarListaDecisoes();
    }
}

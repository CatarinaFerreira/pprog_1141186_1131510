/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

/**
 *
 * @author Catarina
 */

public class PaginaInicial extends JPanel {
        
    private static final ImageIcon ICON = 
            new ImageIcon(PaginaInicial.class.getResource("/imagem/Abstraction-light-neon-glitter-hd-wallpapers.jpg"));
    
    /**
     * Constrói a pagina inicial da Janela
     */
    public PaginaInicial() {
        super();
    }

    /**
     * Altera a forma com a componente é colocada na Janela
     * @param g 
     */
    
    @Override
    public void paintComponent(Graphics g) {
        
        super.paintComponent(g);
        
        Dimension dimensaoPainel = getSize(); 	              
        double largura = dimensaoPainel.getWidth();
        double altura = dimensaoPainel.getHeight();
        
        Image img2 = ICON.getImage().getScaledInstance(
                (int) largura, 
                (int) altura, 
                Image.SCALE_SMOOTH);
        
        Image img3 = new ImageIcon(img2).getImage();
        
        g.drawImage(img3, 0, 0, this);
        
    }
}

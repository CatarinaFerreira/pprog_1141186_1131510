/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.List;
import model.CentroExposicoes;
import model.Utilizador;

/**
 *
 * @author Catarina
 */

public class ConfirmarRegistoUtilizadorController {

    private final CentroExposicoes m_ce;
    private Utilizador m_utilizadorAConfirmar;
    
    /**
     * Construtor de um ConfirmarRegistoUtilizador que recebe por pârametro o centro de exposicoes.
     *
     * @param ce centro de exposicoes.
     */
    
    public ConfirmarRegistoUtilizadorController(CentroExposicoes ce) {
        this.m_ce=ce;
    }    
    
    public List<Utilizador> iniciaConfirmacaoUtilizador() {
        return m_ce.getRegistoUtilizadores().getUtilizadoresNaoRegistados();
    }
    
    public Utilizador getUtilizadorInfo(String uId) {
        m_utilizadorAConfirmar = m_ce.getRegistoUtilizadores().getUtilizadorInfo(uId);
        return m_utilizadorAConfirmar;
    }
    
    public void confirmaRegistoUtilizador() {
        m_ce.getRegistoUtilizadores().confirmaRegistoUtilizador(m_utilizadorAConfirmar);
    }   
}

package controller;

import java.util.List;
import model.Atribuicao;
import model.CentroExposicoes;
import model.Exposicao;
import model.MecanismoAtribuicao;
import model.RegistoMecanismos;
import model.Utilizador;

/**
 *
 * @author Catarina
 */

public class AtribuirCandidaturaController {

    private CentroExposicoes ce;  
    private Exposicao e;
    private MecanismoAtribuicao m;
    private List<Atribuicao> la;
    
    /**
     * 
     * @param ce CentroExposicoes. 
     */
    
    public AtribuirCandidaturaController(CentroExposicoes ce) {
        this.ce=ce;
    }
    
    public List<Exposicao> getListaExposicoes(Utilizador u) {
        return ce.getRegistoExposicoes().getListaExposicoesOrganizador(u);
    }

    public void setExposicao(Exposicao e) {
        this.e=e;
    }

    public List<MecanismoAtribuicao> getListaMecanismos() {
        RegistoMecanismos rm = ce.getRegistoMecanismos();
        return rm.getListaMecanismos();
    }

    public void setMecanismo(MecanismoAtribuicao m) {
        this.m=m;
    }

    public boolean registerAtribuicoes() {
        boolean result = m!=null && e!=null;
        if (result)
        {
            result = m.registerAtribuicoes(la,e);
        }
        return result;
    }
    
    public List<Atribuicao> getListaAtribuicoes() {
        la = m.getListaAtribuicoes(e);
        return la;
    }
}

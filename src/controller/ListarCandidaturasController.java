/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.List;
import model.Candidatura;
import model.CentroExposicoes;

/**
 *
 * @author Catarina
 */

public class ListarCandidaturasController {
    
    private CentroExposicoes ce;
    
    public ListarCandidaturasController(CentroExposicoes ce) {
        this.ce=ce;
    }
    
    public List<Candidatura> getListaCandidaturas() {
        return ce.getRegistoCandidaturas().getListaCandidaturas();
    }
}

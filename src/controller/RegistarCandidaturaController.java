/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.Exposicao;
import model.Candidatura;
import model.CentroExposicoes;
import model.RegistoCandidaturas;
import model.Produto;

/**
 *
 * @author Catarina
 */
public class RegistarCandidaturaController {
    
    private CentroExposicoes ce;
    private Exposicao m_oE;
    private Candidatura m_candidatura;
    private Produto m_produto;
    
    public RegistarCandidaturaController(CentroExposicoes ce) {
        this.ce=ce;
    }
    
    public void novaCandidatura(){
        this.m_candidatura = this.ce.getRegistoCandidaturas().novaCandidatura();
    }
    
    public void setDados(String NomeComercial, String Morada, int Telemovel, double AreaExposicao, int QuantidadeConvites, String nome){
        this.m_candidatura.setNomeComercial(NomeComercial);
        this.m_candidatura.setMorada(Morada);
        this.m_candidatura.setTelemovel(Telemovel);
        this.m_candidatura.setAreaExposicao(AreaExposicao);
        this.m_candidatura.setQuantidadeConvites(QuantidadeConvites);
        this.m_produto = this.m_candidatura.novoProduto();
        this.m_produto.setProduto(nome);
        this.m_candidatura.setProduto(m_produto);
    }
    
    public void addProduto() {
        this.m_produto = this.m_candidatura.novoProduto();
    }
    
    public boolean registaCandidatura() {
        RegistoCandidaturas rc = ce.getRegistoCandidaturas();
        return (rc.validaCandidatura(m_candidatura)) ? rc.registarCandidatura(m_candidatura): false;
    }
    
    public String getCandidaturasString() {
        return this.m_candidatura.toString();
    }
}


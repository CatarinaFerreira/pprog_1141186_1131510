/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.ArrayList;
import java.util.List;
import model.Candidatura;
import model.CentroExposicoes;
import model.Decisao;
import model.Exposicao;

/**
 *
 * @author Catarina
 */
public class ListarDecisoesController {
    
    private CentroExposicoes ce;
    
    public ListarDecisoesController(CentroExposicoes ce)
    {
        this.ce=ce;
    }
    
    public List<Decisao> getListaDecisoes()
    {
        List<Candidatura> list = ce.getRegistoCandidaturas().getListaCandidaturas();
        for (Exposicao e :ce.getRegistoExposicoes().getRegistoExposicoes())
        {
            list.addAll(e.getListaCandidaturas().getListaCandidaturas());
        }
        List<Decisao> result=new ArrayList();
        for (Candidatura element:list)
        {
            result.add(element.getDecisao());
        }
        return result;
    }
}


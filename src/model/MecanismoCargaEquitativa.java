package model;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Catarina
 */

public class MecanismoCargaEquitativa implements MecanismoAtribuicao {

    private ListaAtribuicoes la;
    
    /**
     * Construtor do mecanismo com base no critério de distribuição de carga equitativa pelos FAE.
     */
    
    public MecanismoCargaEquitativa() {
        la = new ListaAtribuicoes();
    }
    
    /**
     * Acede à lista de atribuições.
     * 
     * @param e exposicao.
     * @return 
     */
    
    @Override
    public List<Atribuicao> getListaAtribuicoes(Exposicao e) {
        List<Candidatura> candidaturas = e.getListaCandidaturas().getListaCandidaturas();
        List<FAE> faes = e.getListaFAE().getListaFaes();
        int candidaturasPerFaes = candidaturas.size()/faes.size();
        int leftoverCandidaturas = candidaturas.size()%faes.size();
        List<Atribuicao> result = new ArrayList();
        int offset;
        int i;
        for (i=0;i<faes.size();i++)
        {
            offset=candidaturasPerFaes*i;
            defineAssociacoes(faes.get(i), candidaturas,offset,candidaturasPerFaes,result);
        }
        
        offset=i*candidaturasPerFaes;
        for (i=0;i<leftoverCandidaturas;i++)
        {
            defineAssociacoes(faes.get(i), candidaturas,offset,1,result);
        }
        return result;
    }

    /**
     * Método que regista as atribuições.
     * 
     * @param la lista atribuição.
     * @param e exposição.
     * @return 
     */
    
    @Override
    public boolean registerAtribuicoes(List<Atribuicao> la,Exposicao e) {
        boolean result = this.la.registerAtribuicoes(la);
        if (result) {
            e.setListaAtribuicoes(this.la);
            this.la=null;
        }
        return result;
    }

    /**
     * Define as associações .
     * 
     * @param fae FAE.
     * @param candidaturas lista candidaturas.
     * @param offset serve como contador (marca o início do array).
     * @param maxLength comprimento total do array.
     * @param list lista de atribuições.
     */
    
    private void defineAssociacoes(FAE fae,List<Candidatura> candidaturas,int offset,int maxLength,List<Atribuicao> list) {
        int i=0;
        for (i=0;i<maxLength;i++) {
            Atribuicao a = la.newAtribuicao(candidaturas.get(i+offset), fae);
            if (la.validateAtribuicoes(a)) {
                list.add(a);
            }
        }
    }

    /**
     * Acede à descrição do mecanismo.
     * @return 
     */
    
    @Override
    public String getDescricao() {
        return "Carga equitativa de candidaturas por FAE";
    }
    
    /**
     * Devolve a descrição textual do mecanismo.
     * @return 
     */
    
    @Override
    public String toString(){
        return getDescricao();
    }   
}

package model;

/**
 *
 * @author Catarina
 */

public class Atribuicao {

    /**
     * Variável de instância que guarda a candidatura a ser, posteriormente,
     * associada a um FAE.
     */
    private Candidatura candidatura;

    /**
     * Variável de instância que guarda o FAE a ser, posteriormente, associado a
     * uma candidatura.
     */
    private FAE fae;

    /**
     * Construtor de uma atribuição que recebe por pârametro a candidatura e o
     * FAE.
     *
     * @param c candidatura a uma exposição.
     * @param fae fae.
     */
    public Atribuicao(Candidatura c, FAE fae) {
        this.candidatura = c;
        this.fae = fae;
    }

    public Atribuicao() {

    }

    /**
     * @return the fae
     */
    public FAE getFae() {
        return fae;
    }

    /**
     * @return the candidatura
     */
    public Candidatura getCandidatura() {
        return candidatura;
    }

    /**
     * @param fae the fae to set
     */
    public void setFae(FAE fae) {
        this.fae = fae;
    }

    /**
     * @param candidatura the candidatura to set
     */
    public void setCandidatura(Candidatura candidatura) {
        this.candidatura = candidatura;
    }

    public String toString() {
        return String.format("FAE:" + fae.toString(), "Candidatura:" + candidatura.toString());
    }
}

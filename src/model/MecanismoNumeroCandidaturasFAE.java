package model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Catarina
 */

public class MecanismoNumeroCandidaturasFAE implements MecanismoAtribuicao{

    private ListaAtribuicoes la;
    
    /**
     * Número de candidaturas a ser utilizado neste mecanismo
     */
    
    private final int numCandidaturas;
    
    /**
     * Construtor do mecanismo que tem por base o número de FAE por candidaturas.
     * @param numCandidaturas número de candidaturas utilizado no mecanismo.
     */
    
    public MecanismoNumeroCandidaturasFAE(int numCandidaturas) {
        la = new ListaAtribuicoes();
        this.numCandidaturas=numCandidaturas;
    }
    
    /**
     * Acede à lista de atribuições.
     * @param e Exposicao.
     * @return 
     */
    
    @Override
    public List<Atribuicao> getListaAtribuicoes(Exposicao e) {
        List<Candidatura> candidaturas = e.getListaCandidaturas().getListaCandidaturas();
        List<FAE> faes = e.getListaFAE().getListaFaes();
        List<Atribuicao> result = new ArrayList();
        for (Candidatura element:candidaturas) {
            defineAssociacoes(element, faes,result);
        }
        return result;
    }

    /**
     * Regista as atribuições.
     * 
     * @param la lista atribuição.
     * @param e exposicao.
     * @return 
     */
    
    @Override
    public boolean registerAtribuicoes(List<Atribuicao> la,Exposicao e) {
        boolean result = this.la.registerAtribuicoes(la);
        if (result) {
            e.setListaAtribuicoes(this.la);
            this.la=null;
        }
        return result;
    }

    /**
     * Define as associações.
     * 
     * @param c Candidatura.
     * @param faes lista de FAE.
     * @param list lista de atribuições.
     */
    
    private void defineAssociacoes(Candidatura c,List<FAE> faes,List<Atribuicao> list) {
        int i;
        Integer[] arr = new Integer[faes.size()];
        for (i=0;i<arr.length;i++) {
            arr[i]=i;
        }
        Collections.shuffle(Arrays.asList(arr));
        for (i=0;i<faes.size() && i<numCandidaturas;i++) {
            Atribuicao a = la.newAtribuicao(c, faes.get(arr[i]));
            if (la.validateAtribuicoes(a))
            {
                list.add(a);
            }
        }
    }

    /**
     * Acede à descrição do mecanismo.
     * @return 
     */
    
    @Override
    public String getDescricao() {
        return "Número Candidaturas por FAE";
    }
    
    /**
     * Devolve a descrição textual do mecanismo.
     * @return 
     */
    
    @Override
    public String toString() {
        return getDescricao();
    }    
}

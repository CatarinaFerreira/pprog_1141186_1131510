/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Catarina
 */
public class Expositor {
    
    /**
     * Variável de instância que guarda o nome do expositor.
     */
    
    private String nome;
    
    /**
     * Variável de instância que guarda a morada do expositor.
     */
    
    private String morada;

    /**
     * Construtor de um expositor.
     * 
     */
    
    public Expositor() {
	this.nome = new String();
	this.morada = new String();
    }
	
    /**
     * Altera o nome do expositor (empresa), recebendo um novo por parâmetro.
     * 
     * @param empresa novo nome empresa (expositor) que pretende expor produto.
     */
    
    public void setNome(String empresa) {
	this.nome = empresa;
    }

    /**
     * Altera a morada do expositor (empresa), recebendo uma nova por parâmetro
     * 
     * @param morada nova morada expositor (empresa).
     */
    
    public void setMorada(String morada) {
	this.morada = morada;
    }
}

package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Catarina
 */

public class RegistoMecanismos {

    private List<MecanismoAtribuicao> mecanismos;
    
    /**
     * Constrói uma instância RegistoMecanismos.
     */
    
    public RegistoMecanismos() {
        mecanismos = new ArrayList();
    }
    
    /**
     * Acede à lista que contém os mecanismos de atribuição definidos.
     * @return 
     */
    
    public List<MecanismoAtribuicao> getListaMecanismos() {
        return Collections.unmodifiableList(mecanismos);
    }
    
    /**
     * Adiciona um mecanismo.
     * 
     * @param mecanismo mecanismoAtribuição.
     * @return 
     */

    public boolean addMecanismo(MecanismoAtribuicao mecanismo) {
        return this.mecanismos.add(mecanismo);
    }
}

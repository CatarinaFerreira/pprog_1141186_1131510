/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Catarina
 */

public final class Organizador {

    private Utilizador m_oUtilizador;

    /**
     * Constrói uma instância Organizador sem parâmetros.
     */
    
    public Organizador() {
    }
    
    /**
     * Constrói uma instância Organizador que recebe como parâmetro o utilizador.
     * @param u Utilizador.
     */

    public Organizador(Utilizador u) {
        setUtilizador(u);
    }

    /**
     * Altera o utilizador, recebendo um novo por parâmetro.
     * @param u novo Utilizador.
     */
    
    public void setUtilizador(Utilizador u) {
        this.m_oUtilizador = u;
    }

    /**
     * Valida o organizador. 
     * @return 
     */
    
    public boolean valida() {
        // Introduzir as validações aqui
        return true;
    }

    /**
     * 
     * @param other
     * @return 
     */
    
    @Override
    public boolean equals(Object other) {
        boolean result = other != null && getClass().equals(other.getClass());
        if (result) {
            Organizador o = getClass().cast(other);
            result = (this == other) || (m_oUtilizador.equals(o.m_oUtilizador));
        }
        return result;
    }

    /**
     * Devolve a descrição textual do organizador.
     *
     * @return
     */
    @Override
    public String toString() {
        return this.m_oUtilizador.toString();
    }

    /**
     * Verifica se o organizador é um utilizador.
     *
     * @param u Utilizador.
     * @return
     */
    public boolean isUtilizador(Utilizador u) {
        if (this.m_oUtilizador != null) {
            return this.m_oUtilizador.equals(u);
        }
        return false;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Catarina
 */

public class ListaFAE {
    
    private List<FAE> faes;
    
    /**
    * Método que cria uma lista de FAE.
    */
    
    public ListaFAE() {
        faes=new ArrayList();
    }
    
    /**
     * Método que cria um novo FAE.
     * 
     * @param u Utilizador.
     * @return 
     */
    
    public FAE novoFAE(Utilizador u) {
        return new FAE(u);
    }
    
    /**
     * Acede à lista de FAE da exposição.
     * 
     * @return 
     */
    public List<FAE> getListaFaes() {
        return Collections.unmodifiableList(faes);
    }
    
    /**
     * Verifica se o utilizador registado é um FAE.
     * @param u Utilizador.
     * @return 
     */
    
    public boolean hasFAE (Utilizador u) {
        for(int i = 0; i<this.faes.size();i++) {
            if(this.faes.get(i).isUtilizador(u)) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Regista um novo FAE.
     * 
     * @param fae FAE.
     * @return 
     */
    
    public boolean registerFAE(FAE fae) {
        return (fae.valida()) ? faes.add(fae): false;
    }
    
    /**
     * Valida o FAE.
     * 
     * @param fae FAE.
     * @return 
     */
    
    public boolean validaFAE(FAE fae) {
        return !faes.contains(fae);
    }
    
    public FAE getFAE(Utilizador u) {
        for (FAE fae : this.faes) {
            if (fae.getUtilizador().equals(u)) {
                return fae;
            }
        }
        return null;
    }
}

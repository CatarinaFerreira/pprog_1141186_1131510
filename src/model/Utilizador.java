/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Catarina
 */
public final class Utilizador {
    
    /**
     * Variável de instância que guarda o nome do utilizador.
     */
    private String m_sNome;

    /**
     * Variável de instância que guarda o email do utilizador.
     */
    private String m_sEmail;

    /**
     * Variável de instância que guarda o username do utilizador.
     */
    private String m_sUserName;

    /**
     * Variável de instância que guarda a password do utilizador.
     */
    private String m_sPassword;

    /**
     * Variável de instância que verifica
     */
    private Boolean m_bRegistado;

    /**
     * Constrói uma instância utilizador
     */
    public Utilizador() {
    }

    /**
     * Constrói uma instância Utilizador, recebendo por o seu nome, email e
     *
     * @param sNome nome do utilizador.
     * @param sEmail email do utilizador.
     * @param sUserName username do utilizador.
     * @param sPassword password do utilizador.
     * @param bRegistado verifica registo utilizador.
     */
    
    public Utilizador(String sNome, String sEmail, String sUserName, String sPassword, Boolean bRegistado) {
        setNome(sNome);
        setEmail(sEmail);
        setUsername(sUserName);
        setPassword(sPassword);
        setRegistado(bRegistado);
    }   

    public Utilizador(String sNome, String sEmail, String sUserName, String sPassword) {
        setNome(sNome);
        setEmail(sEmail);
        setUsername(sUserName);
        setPassword(sPassword);
        setRegistado(false);
    }

    public boolean hasID(String strId) {
        return m_sUserName.equalsIgnoreCase(strId);
    }

    /**
     * Método que acede ao ID do utilizador.
     *
     * @return username do utilizador.
     */
    public String getID() {
        return m_sUserName;
    }

    /**
     * Método que acede ao nome do utilizador.
     *
     * @return nome do utilizador.
     */
    public String getNome() {
        return m_sNome;
    }

    /**
     * Método que altera o nome do utilizador, recebendo um novo por parâmetro.
     *
     * @param nome novo nome.
     */
    public final void setNome(String nome) {
        this.m_sNome = nome;
    }

    /**
     * Método que acede ao email do utilizador.
     *
     * @return email do utilizador.
     */
    public String getEmail() {
        return m_sEmail;
    }

    /**
     * Método que altera o email do utilizador, recebendo um novo por parâmetro.
     *
     * @param email novo email.
     */
    public void setEmail(String email) {
        this.m_sEmail = email;
    }

    /**
     * Método que acede ao username do utilizador.
     *
     * @return username do utilizador.
     */
    public String getUsername() {
        return m_sUserName;
    }

    /**
     * Método que altera o username do utilizador, recebendo um novo por parâmetro.
     *
     * @param username novo username.
     */
    public void setUsername(String username) {
        this.m_sUserName = username;
    }

    /**
     * Método que acede à password do utilizador.
     *
     * @return password do utilizador.
     */
    public String getPassword() {
        return m_sPassword;
    }

    /**
     * Método que altera a password do utilizador, recebendo uma nova por parâmetro.
     *
     * @param password nova password.
     */
    public void setPassword(String password) {
        this.m_sPassword = password;
    }
    
    public Boolean getRegistado() {
        return m_bRegistado;
    }
   
    public void setRegistado(Boolean registado) {
        this.m_bRegistado = registado;
    }

    /**
     * Devolve a descrição textual do utilizador.
     *
     * @return
     */
    @Override
    public String toString() {
        return String.format("[%s, %s, %s]", m_sNome, m_sEmail, m_sUserName, m_sPassword);
    }

    /**
     * Método que valida o utilizador.
     *
     * @return
     */
    public boolean valida() {
        return true;
    }
    
    /**
     * Verifica a unicidade do utilizador.
     * @param other
     * @return 
     */

    @Override
    public boolean equals(Object other) {
        boolean result = other != null && getClass().equals(other.getClass());
        if (result) {
            Utilizador o = getClass().cast(other);
            result = (this == other) || (m_sEmail.equals(o.m_sEmail) && m_sUserName.equals(o.m_sUserName));
        }
        return result;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Catarina
 */
public class Candidatura {

    /**
     * Variável de instância que guarda o nome comercial do expositor.
     */
    private String m_sNomeComercial;

    /**
     * Variável de instância que guarda a morada do expositor.
     */
    private String m_sMorada;

    /**
     * Variável de instância que guarda o telemóvel do expositor.
     */
    private int m_iTelemovel;

    /**
     * Variável de instância que guarda o valor da área de exposição pretendida
     * pelo expositor.
     */
    private double m_dAreaExposicao;

    /**
     * Variável de instância que guarda o número de convites pretendidos pelo
     * expositor.
     */
    private int m_iQuantidadeConvites;

    /**
     * Variável de instância que guarda o nome do produto que o expositor
     * pretende expor.
     */
    private Produto m_pNome;

    /**
     * Variável de atribuição que guarda, por omissão, o nome comercial do
     * expositor.
     */
    private static final String NOMECOMERCIAL_POR_OMISSAO = "";

    /**
     * Variável de atribuição que guarda, por omissão, a morada do expositor.
     */
    private static final String MORADA_POR_OMISSAO = "";

    /**
     * Variável de atribuição que guarda, por omissão, o número de telemóvel do
     * expositor.
     */
    private static final int TELEMOVEL_POR_OMISSAO = 0;

    /**
     * Variável de atribuição que guarda, por omissão, o valor da área de
     * exposição pretendida pelo expositor.
     */
    private static final float AREAEXPOSICAO_POR_OMISSAO = 0;

    /**
     * Variável de atribuição que guarda, por omissão, o número de convites
     * pretendidos pelo expositor.
     */
    private static final int QUANTIDADE_CONVITES_POR_OMISSAO = 0;

    private Decisao decisao;

    /**
     * Construtor de uma candidatura que recebe por pârametro o nome comercial
     * do expositor, a morada, o número de telemóvel, o valor da área de
     * exposição pretendida, o número de convites necessários e o nome do
     * produto a expor.
     *
     * @param m_sNomeComercial nome comercial do expositor.
     * @param m_sMorada morada.
     * @param m_iTelemovel número de telemóvel.
     * @param m_dAreaExposicao área de exposição.
     * @param m_iQuantidadeConvites número de convites.
     * @param m_pNome produto.
     */
    public Candidatura(String m_sNomeComercial, String m_sMorada, int m_iTelemovel, double m_dAreaExposicao, int m_iQuantidadeConvites, String m_pNome) {
        this.m_sNomeComercial = m_sNomeComercial;
        this.m_sMorada = m_sMorada;
        this.m_iTelemovel = m_iTelemovel;
        this.m_dAreaExposicao = m_dAreaExposicao;
        this.m_iQuantidadeConvites = m_iQuantidadeConvites;
        this.m_pNome = new Produto(m_pNome);
    }

    /**
     * Construtor de uma candidatura sem parâmetros.
     */
    public Candidatura() {
        m_sNomeComercial = NOMECOMERCIAL_POR_OMISSAO;
        m_sMorada = MORADA_POR_OMISSAO;
        m_iTelemovel = TELEMOVEL_POR_OMISSAO;
        m_dAreaExposicao = AREAEXPOSICAO_POR_OMISSAO;
        m_iQuantidadeConvites = QUANTIDADE_CONVITES_POR_OMISSAO;
        this.m_pNome = new Produto();
    }

    /**
     * Define o nome comercial como ID (identificação) da candidatura.
     *
     * @param nomeComercial.
     * @return m_sNomeComercial.
     */
    public boolean hasID(String nomeComercial) {
        return m_sNomeComercial.equalsIgnoreCase(nomeComercial);
    }

    /**
     * Acede ao ID (identificação) da candidatura.
     *
     * @return m_sNomeComercial.
     */
    public String getID() {
        return m_sNomeComercial;
    }

    /**
     * Acede ao nome nome comercial do expositor.
     *
     * @return the m_sNomeComercial.
     */
    public String getNomeComercial() {
        return m_sNomeComercial;
    }

    /**
     * Altera o nome comercial do expositor.
     *
     * @param m_sNomeComercial
     */
    public void setNomeComercial(String m_sNomeComercial) {
        this.m_sNomeComercial = m_sNomeComercial;
    }

    /**
     * Acede à morada do expositor.
     *
     * @return the m_sMorada.
     */
    public String getMorada() {
        return m_sMorada;
    }

    /**
     * Altera a morada do expositor.
     *
     * @param m_sMorada.
     */
    public void setMorada(String m_sMorada) {
        this.m_sMorada = m_sMorada;
    }

    /**
     * Acede ao número de telemóvel do expositor.
     *
     * @return the m_iTelemovel.
     */
    public int getTelemovel() {
        return m_iTelemovel;
    }

    /**
     * Altera o número de telemóvel do expositor.
     *
     * @param m_iTelemovel.
     */
    public void setTelemovel(int m_iTelemovel) {
        this.m_iTelemovel = m_iTelemovel;
    }

    /**
     * Devolve a avaliação da candidatura.
     *
     * @return a avaliação da candidatura.
     */
    public Decisao getDecisao() {
        return decisao;
    }

    public void setDecisao(Decisao deci) {
        if (deci == (null)) {
            throw new IllegalArgumentException("A avaliação não foi escrita!");
        }
        this.decisao = deci;
    }

    /**
     * Acede ao valor para a área de exposição pretendido pelo expositor.
     *
     * @return the m_dAreaExposicao.
     */
    public double getAreaExposicao() {
        return m_dAreaExposicao;
    }

    /**
     * Altera o valor para a área de exposição pretendido pelo expositor.
     *
     * @param m_dAreaExposicao.
     */
    public void setAreaExposicao(double m_dAreaExposicao) {
        this.m_dAreaExposicao = m_dAreaExposicao;
    }

    /**
     * Acede ao número de convites pretendido pelo expositor.
     *
     * @return the m_iQuantidadeConvites.
     */
    public int getQuantidadeConvites() {
        return m_iQuantidadeConvites;
    }

    /**
     * Altera o número de convites pretendido pelo expositor.
     *
     * @param m_iQuantidadeConvites.
     */
    public void setQuantidadeConvites(int m_iQuantidadeConvites) {
        this.m_iQuantidadeConvites = m_iQuantidadeConvites;
    }

    /**
     * Acede ao nome do produto a expor.
     *
     * @return the m_pNome.
     */
    public Produto getProduto() {
        return m_pNome;
    }

    /**
     * Altera o nome do produtor a expor.
     *
     * @param m_pProduto.
     */
    public void setProduto(Produto m_pProduto) {
        this.m_pNome = m_pProduto;
    }

//  /**
//     * Acede à lista de decisões tomadas.
//     *
//     * @return m_lDecisoes
//     */
//    public List<Decisao> getDecisoes() {
//        return m_lDecisoes;
//    }
    /**
     * Devolve a descrição textual de uma candidatura.
     *
     * @return String com a descrição da candidatura.
     */
    @Override
    public String toString() {
        return String.format("[%s, %s, %d, %f, %d, %s]", m_sNomeComercial, m_sMorada, m_iTelemovel, m_dAreaExposicao, m_iQuantidadeConvites, m_pNome);
    }

    /**
     * Cria um novo produto.
     *
     * @return Produto().
     */
    public Produto novoProduto() {
        return new Produto();
    }

    /**
     * Valida a candidatura.
     *
     * @return boolean true/false.
     */
    public boolean valida() {
        if (!(this.m_sNomeComercial.equalsIgnoreCase("") || this.m_sMorada.equalsIgnoreCase("") || this.m_iTelemovel <= 0 || this.m_iQuantidadeConvites < 0 || this.m_dAreaExposicao < 0)) {
            return true;
        }
        return false;
    }

    /**
     * Verifica se uma candidatura se encontra completa.
     *
     * @param c Candidatura.
     * @return boolean true/false.
     */
    public boolean equals(Candidatura c) {
        if (this != c) {
            return true;
        }
        if (c != null) {
            return hasID(m_sNomeComercial);
        }
        return false;
    }
}

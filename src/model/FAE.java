/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Catarina
 */
public class FAE {
    
    private Utilizador m_oUFae;
    
    /**
    * Construtor de um FAE sem parâmetros.
    */
    public FAE() {
    }
    
    /**
     * Construtor de um FAE que recebe por parâmetro o utilizador.
     * 
     * @param u 
     */
    
    public FAE(Utilizador u) {
      this.m_oUFae = u;
    }
    
    
    /**
     * Valida o FAE.
     * @return 
     */
    
    public boolean valida() {
        if(!this.getUtilizador().getNome().equalsIgnoreCase("") || this.getUtilizador().getEmail().equalsIgnoreCase("") || 
                this.getUtilizador().getUsername().equalsIgnoreCase("") || this.getUtilizador().getPassword().equalsIgnoreCase("")){
        return true;
        }
        return false;
    }
    
    /**
     * Verifica se o FAE é um utilizador .
     * 
     * @param u Utilizador.
     * @return 
     */
    public boolean isUtilizador(Utilizador u) {
        if (this.getUtilizador() != null) {
            return this.getUtilizador().equals(u);
        }
        return false;
    }
    
    /**
     * Devolve a descrição textual do FAE.
     * @return 
     */
    
    @Override
    public String toString() {
        return this.m_oUFae != null ? this.m_oUFae.toString() : null;
    }   

    /**
     * Acede ao utlizador (FAE).
     * 
     * @return the m_oUFae.
     */
    public Utilizador getUtilizador() {
        return m_oUFae;
    }
}

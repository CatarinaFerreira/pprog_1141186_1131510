/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Catarina
 */

public class RegistoUtilizador {
    
    List<Utilizador> m_lUtilizadores;
    
    /**
     * Constrói uma instância RegistoUtilizadores.
     */
    
    public RegistoUtilizador(){
        m_lUtilizadores=new ArrayList<>();
    }
    
    /**
     * Método que permite criar um novo utilizador.
     * 
     * @return novo utilizador.
     */
    
    public Utilizador novoUtilizador(){
        return new Utilizador();
    }
    
    /**
     * Método que valida os dados do utilzador.
     * 
     * @param email email do utilizador.
     * @param username username do utilizador.
     * @return 
     */
    
    public boolean validaDadosUtilizador( String email, String username) {
        for (int i = 0; i < m_lUtilizadores.size(); i++) {
            if (email.equalsIgnoreCase(m_lUtilizadores.get(i).getEmail() ) || username.equalsIgnoreCase(m_lUtilizadores.get(i).getUsername())) {
                return true;
            } 
        }
            return false;
    }
    
    /**
     * Método que valida o utilizador.
     * 
     * @param u Utilizador.
     */
    
    private boolean validaUtilizador(Utilizador u) {
        if(u.valida()==true){
        if(!m_lUtilizadores.contains(u)){
            return true;
        }}
        return false;
    }
    
    /**
     * Metodo que permite registar um utilizador, recebendo-o por parâmetro.
     * 
     * @param u utilizador a registar.
     * @return true se registar o novo utilizador, caso contrário false.
     */
   public boolean registaUtilizador(Utilizador u) {
        if (validaUtilizador(u)==true) {
            addUtilizador(u);
            return true;
        }
        return false;
    }
   
   /**
    * Adiciona o utilizador.
    * @param u Utilizador.
    */
   private void addUtilizador(Utilizador u) {
        m_lUtilizadores.add(u);
    }
   
   /**
    * Acede aos utilizadores registados.
    * @return 
    */
   
    public List<Utilizador> getUtilizadores() {
        return this.m_lUtilizadores;
    }
    
   /**
    * Acede à informação do utilizador.
    * 
    * @param strId ID do utilizador.
    * @return 
    */
    
   public Utilizador getUtilizadorInfo(String strId) {
        for(Utilizador u:this.m_lUtilizadores)
        {
            if (u.hasID(strId))
                return u;
        }
        return null;
    
    }
   
   /**
    * Acede aos utilizadores não registados.
    * 
    * @return 
    */
 
    public List<Utilizador> getUtilizadoresNaoRegistados() {
        List<Utilizador> lUsers = new ArrayList<Utilizador>();

        for (Utilizador u : m_lUtilizadores) {
            if (!u.getRegistado()) {
                lUsers.add(u);
            }
        }
        return lUsers;
    }

    /**
     * Método que confirma o registo do utilizador.
     * 
     * @param u Utilizador.
     */
    
    public void confirmaRegistoUtilizador(Utilizador u) {
        u.setRegistado(Boolean.TRUE);
    }       
}

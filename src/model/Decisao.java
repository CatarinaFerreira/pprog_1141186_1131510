/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Catarina
 */
public class Decisao implements Serializable {

    private String m_decisao;
    private String m_justificacao;

    public Decisao(String decisao, String justificacao) {
        this.m_decisao = decisao;
        this.m_justificacao = justificacao;
    }

    public Decisao() {
        this.m_decisao = "sem decisao";
        this.m_justificacao = "sem justificação";
    }

    public String getM_decisao() {
        return m_decisao;
    }

    public String getM_justificacao() {
        return m_justificacao;
    }

    public void setM_decisao(String m_decisao) {
        if (m_decisao.equalsIgnoreCase(null) || m_decisao.length() == 0) {
            throw new IllegalArgumentException("A decisão está inválida!");
        }
        this.m_decisao = m_decisao;
    }

    public void setM_justificacao(String m_justificacao) {
        if (m_justificacao.equalsIgnoreCase(null) || m_decisao.length() == 0) {
            throw new IllegalArgumentException("A justificação está inválida!");
        }
        this.m_justificacao = m_justificacao;

    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Decisao other = (Decisao) obj;
        if (!Objects.equals(this.m_decisao, other.m_decisao)) {
            return false;
        }
        if (!Objects.equals(this.m_justificacao, other.m_justificacao)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Decisao{"
                + "Decisao=" + m_decisao
                + "Justificacao=" + m_justificacao + '}';
    }
}

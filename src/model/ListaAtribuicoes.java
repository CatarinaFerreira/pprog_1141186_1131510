package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Catarina
 */

public class ListaAtribuicoes {

    private List<Atribuicao> atribuicoes;
    
    /**
     * Método que cria uma lista de atribuições.
     */   
    
    public ListaAtribuicoes() {
        atribuicoes=new ArrayList();
    }
    
    /**
     * Método que devolve a lista de atribuições.
     * @return 
     */
    
    public List<Atribuicao> getListaAtribuicoes() {
        return Collections.unmodifiableList(atribuicoes);
    }

    /**
     * Método que regista as atribuições na lista de atribuições.
     * 
     * @param la lista de atribuições.
     * @return 
     */
    
    public boolean registerAtribuicoes(List<Atribuicao> la) {
        return atribuicoes.addAll(la);
    }

    /**
     * Método que faz uma nova atribuição recebendo por parâmetro a candidatura e o FAE
     * @param c Candidatura.
     * @param fae FAE.
     * @return 
     */
    
    public Atribuicao newAtribuicao(Candidatura c, FAE fae) {
        return new Atribuicao(c,fae);
    }

    /**
     * Método que valida as atribuições efetuadas.
     * @param a Atribuição.
     * @return 
     */
    
    public boolean validateAtribuicoes(Atribuicao a) {
        return !atribuicoes.contains(a);
    }
}

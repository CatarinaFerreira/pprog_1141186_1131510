package model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Catarina
 */

public class RegistoExposicoes {
 
    //private List<Exposicao> exposicoes;
    
    private final List<Exposicao> m_lExposicoes;  

    /**
     * Constrói uma instância RegistoExposicoes
     */
    
    public RegistoExposicoes() {
        m_lExposicoes = new ArrayList<>();
    }

    /**
     * Metodo que permite criar uma nova exposição
     *
     * @return novo utilizador.
     */
    
    public Exposicao novaExposicao() {
        return new Exposicao();
    }

    /**
     * Método que valida a exposição
     * 
     * @param e exposicao
     * @return 
     */
    
    public boolean validaExposicao(Exposicao e) {
        if(e.valida()==true){
        if (!m_lExposicoes.contains(e)) {
            return true;
        }}
        return false;
    }

    /**
     * Metodo que permite registar uma exposição, recebendo-a por parâmetro
     *
     * @param e Exposicao
     * @return true se registar nova exposicao, caso contrário false.
     */
    
    public boolean registaExposicao(Exposicao e) {
        if (validaExposicao(e)) {
            addExposicao(e);
            return true;
        }
        return false;
    }
    
    /**
     * Metodo que adiciona uma exposição, recebendo-a por parâmetro
     *
     * @param e Exposicao
     */

    private void addExposicao(Exposicao e) {
        m_lExposicoes.add(e);
    }

    /**
     * Acede ao registo de exposições
     * @return 
     */
    
    public List<Exposicao> getRegistoExposicoes() {
        return m_lExposicoes;
    }

    /**
     * Acede à informação da exposição
     * 
     * @param eId ID exposicao
     * @return 
    */
    
    public Exposicao getExposicaoInfo(int eId) {
        for (Exposicao e : m_lExposicoes) {
            if (eId == e.getId()) {
                return e;
            }
        }
        return null;
    }

    public List<Exposicao> getListaExposicoesDoFAE(Utilizador u) {
        List<Exposicao> l_ExpDoFAE = new ArrayList();

        for (Exposicao m : this.m_lExposicoes) {
            if (m.getListaFAE().hasFAE(u)) {
                l_ExpDoFAE.add(m);
            }

        }
        return l_ExpDoFAE;
    }

    public List<Exposicao> getListaExposicoesDoOrganizador(Utilizador u) {
        List<Exposicao> leOrganizador = new ArrayList();

        for (Exposicao m : this.m_lExposicoes) {
            if (m.getListaOrganizadores().hasOrganizador(u)) 
            {
                leOrganizador.add(m);
            }

        }

        return leOrganizador;
    }
    
     
    public List<Exposicao> getExposicaoOrganizador(String strId,RegistoUtilizador ru)
    {
        List<Exposicao> leOrganizador = new ArrayList<Exposicao>();
        //RegistoUtilizador ru = new RegistoUtilizador(); Isto devia usar o registo de utilizadores
        //Passado por parâmetro. Esse registo deve vir do centro de exposições.
        Utilizador u = ru.getUtilizadorInfo(strId);
        
        if(u != null )
        {
            for( Iterator<Exposicao> it = this.m_lExposicoes.listIterator(); it.hasNext(); )
            {
                Exposicao e = it.next();
                
                if (e.getListaOrganizadores().hasOrganizador(u)==true)
                    leOrganizador.add(e);
            }
        }
        return leOrganizador;
    }
    
    /**
     * Inicia o registo de exposições com valores de teste.
     * @param ru registo utilizador.
     */
    
    protected void fillInData(RegistoUtilizador ru) {
        Calendar beginDate;
        Calendar endDate;
        Utilizador u = ru.getUtilizadores().get(0);
        for (Integer i = 1; i <= 1; i++) {
            beginDate = Calendar.getInstance();
            beginDate.add(Calendar.WEEK_OF_YEAR, i);
            endDate = (Calendar)beginDate.clone();
            endDate.add(Calendar.DAY_OF_YEAR,1);
            Exposicao e = new Exposicao("Titulo" + i.toString(), "Descrição" + i.toString()
                    ,beginDate.getTime(),endDate.getTime(),"Local"+i);
            
            ListaOrganizadores list = e.getListaOrganizadores();
            Organizador o = list.novoOrganizador();
            o.setUtilizador(u);
            list.registerOrganizador(o);
            
            
            ListaFAE faes = e.getListaFAE();
            FAE fae = faes.novoFAE(u);
            faes.registerFAE(fae);
            
            fae = faes.novoFAE(ru.getUtilizadores().get(1));
            faes.registerFAE(fae);
            
            ListaCandidaturas candidaturas = e.getListaCandidaturas();
            
            generateCandidaturas(candidaturas, 4);
            
            this.m_lExposicoes.add(e);

        }
    }
    
    /**
     * Método com dados para registo de exposição.
     * @param candidaturas lista candidaturas.
     * @param amount quantidade de candidaturas.
     */
    
    private void generateCandidaturas(ListaCandidaturas candidaturas,int amount) {
        for (int i=1;i<=amount;i++) {
            Candidatura can = candidaturas.novaCandidatura();
            can.setNomeComercial("ComercialTest"+i);
            can.setMorada("TestAddress"+i);
            can.setAreaExposicao(25);
            can.setQuantidadeConvites(20);
            can.setTelemovel(917121385);
            
            Produto p = can.novoProduto();
            p.setProduto("Test Product"+i);
            
            can.setProduto(p);
            
            candidaturas.registarCandidatura(can);
        }
    }
    
    public List<Exposicao> getListaExposicoesOrganizador(Utilizador u) {
        List<Exposicao> leOrganizador = new ArrayList<Exposicao>();

        //Utilizador u = getUtilizador(strId);
        
        if(u != null ) {
            for( Iterator<Exposicao> it = this.m_lExposicoes.listIterator(); it.hasNext(); ) {
                Exposicao e = it.next();
                
                if (e.getListaOrganizadores().hasOrganizador(u))
                    leOrganizador.add(e);
            }
        }
        return leOrganizador;
    }
}

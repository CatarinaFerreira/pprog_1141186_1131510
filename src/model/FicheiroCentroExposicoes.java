/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 *
 * @author Catarina
 */
public class FicheiroCentroExposicoes {
    
    public static final String NOME = "CentroExposicoes.bin";
    
    /**
     * Método que lê informação a partir de um ficheiro binário e retoma o estado do centro de exposições 
     * quando este foi criado.
     * 
     * @param nomeFicheiro ficheiro binário.
     * @return centroExposicoes.
     */
    public CentroExposicoes ler(String nomeFicheiro) {
        CentroExposicoes centroExposicoes;
        try {
            ObjectInputStream in = new ObjectInputStream(
                    new FileInputStream(nomeFicheiro));
            try {
                centroExposicoes = (CentroExposicoes) in.readObject();
            } finally {
                in.close();
            }
            return centroExposicoes;
        } catch (IOException | ClassNotFoundException ex) {
            return null;
        }
    }
    
    /**
     * Guarda o estado atual do centro de exposições para que, posteriormente, o estado do mesmo possa ser retomado.
     * 
     * @param nomeFicheiro nome do ficheiro a guardar.
     * @param centroexposicoes centroexposicoes.
     * @return true caso tenha exportado com sucesso, false caso contrário.
     * @throws IOException 
     */
    
    public boolean guardar(String nomeFicheiro, CentroExposicoes centroexposicoes) throws IOException {
        try {
            ObjectOutputStream out = new ObjectOutputStream(
                    new FileOutputStream(nomeFicheiro));
            try {
                out.writeObject(centroexposicoes);
            } 
            finally {
                out.close();
            }
            return true;
        } catch (IOException ex) {
            return false;
        }
    }
}

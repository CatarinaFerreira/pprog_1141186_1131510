/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Catarina
 */
public class ListaOrganizadores {   
    
    private List<Organizador> organizadores;
    
    /**
    * Método que cria uma lista de organizadores.
    */
    
    public ListaOrganizadores() {
        organizadores=new ArrayList();
    }
    
    /**
     * Método que cria um novo organizador.
     * 
     * @return 
     */
    
    public Organizador novoOrganizador() {
        return new Organizador();
    }
    
    /**
     * Acede à lista de organizadores da exposição.
     * 
     * @return 
     */
    public List<Organizador> getListaOrganizadores() {
        return Collections.unmodifiableList(organizadores);
    }
    
    /**
     * Verifica se o utilizador registado é um organizador.
     * 
     * @param u Utilizador.
     * @return 
     */
    
    public boolean hasOrganizador (Utilizador u) {
        for(int i = 0; i<this.getListaOrganizadores().size();i++) {
            if(this.organizadores.get(i).isUtilizador(u)) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Método que regista o organizador.
     * 
     * @param org organizador.
     * @return 
     */
    
    public boolean registerOrganizador(Organizador org) {
        return (org.valida()) ? organizadores.add(org): false;
    }
    
    /**
     * Método que valida o organizador inserido.
     * @param org organizador.
     * @return 
     */
    
    public boolean validaOrganizador(Organizador org) {
        return !organizadores.contains(org);
    }
}

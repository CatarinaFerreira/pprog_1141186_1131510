/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Catarina
 */

public class Produto {
    
    /**
     * Variável de instância que guarda o nome do produto a expor
     */
    private String nome;
    
    /**
     * Variável de atribuição que guarda, por omissão, o nome do produto a expor
     */
    private static final String NOME_POR_OMISSAO ="";	
    
    /**
     * Construtor do produto a expor que recebe por parâmetro o nome do mesmo
     * @param nome 
     */
    public Produto(String nome) {
	this.nome = nome;
    }
    
    /**
     * Construtor do produto a expor sem parâmetros
     */
    public Produto() {
        nome = NOME_POR_OMISSAO;
    }
    
    /**
     * Acede ao nome do produto a expor
     * @return nome
     */
    public String getNome() {
        return nome;
    }
    
    /**
     * Altera o nome do produto a expor
     * @param nome
     */
    public void setProduto(String nome) {
	this.nome = nome;
    }
    
    /**
     * Devolve a descrição textual do produto a expor
     * @return nome do produto.
     */
    
    @Override
    public String toString() {
        return String.format("%s", getNome());
    }
    
    
    public Produto (Produto outroProduto){
        nome = outroProduto.nome;
    }
}

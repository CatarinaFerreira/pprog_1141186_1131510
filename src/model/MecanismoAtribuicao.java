package model;

import java.util.List;

public interface MecanismoAtribuicao {

    public List<Atribuicao> getListaAtribuicoes(Exposicao e);

    public boolean registerAtribuicoes(List<Atribuicao> la,Exposicao e);
    
    /**
     * Retorna a descrição do mecanismo, utilizando o polimorfismo.
     * @return 
     */
    
    public String getDescricao();
}

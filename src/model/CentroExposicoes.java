/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Catarina
 */
public class CentroExposicoes {

    private RegistoUtilizador ru;
    //private final List<Exposicao> m_lExposicoes;
    private RegistoExposicoes re;
    private RegistoMecanismos rm;
    private RegistoCandidaturas rc;

    /**
     * Constrói uma instância Centro Exposições
     */
    public CentroExposicoes() {
        this.ru = new RegistoUtilizador();
        //this.m_lExposicoes = new ArrayList<>();
        this.re = new RegistoExposicoes();
        this.rm = new RegistoMecanismos();
        this.rc = new RegistoCandidaturas();
        this.rm.addMecanismo(new MecanismoNumeroCandidaturasFAE(4));
        this.rm.addMecanismo(new MecanismoCargaEquitativa());
        this.rm.addMecanismo(new MecanismoExperienciaFae(this));
        fillInData(); //ver o metodo
        this.re.fillInData(ru);
    }

    /**
     * Constrói uma instância Centro Exposições que recebe por parâmetro a lista
     * de utilizadores, a lista de exposições e a lista de candidaturas.
     *
     * @param lista_Utilizadores ListaUtilizadores.
     * @param lista_Exposicoes ListaExposições.
     * @param lista_Candidaturas ListaCandidaturas.
     */
    /**
     * Devolve o utilizador a utilizar o sistema.
     *
     * @return
     */
    public Utilizador getLoggedInUser() {
        return ru.getUtilizadores().get(0);
    }

    /**
     * Devolve o registo de utilizadores existente.
     *
     * @return ru registo utilizadores.
     */
    public RegistoUtilizador getRegistoUtilizadores() {
        return ru;
    }

    /**
     * Devolve o registo de exposições existente.
     *
     * @return re registo exposições.
     */
    public RegistoExposicoes getRegistoExposicoes() {
        return re;
    }

    /**
     * Devolve o registo de mecanismos de atribuição existentes.
     *
     * @return re registo exposições.
     */
    public RegistoMecanismos getRegistoMecanismos() {
        return rm;
    }

    /**
     * Devolve o registo de candidaturas existentes no sistema.
     *
     * @return rc registo candidaturas.
     */
    public RegistoCandidaturas getRegistoCandidaturas() {
        return this.rc;
    }

    /**
     * Metodo que permite validar a exposição, recebendo-a por parâmetro.
     *
     * @param e Exposicao.
     * @return true se for registado, caso contrário false.
     */
    public boolean validaExposicao(Exposicao e) {
        if (e.valida()) {
            // Introduzir as validações aqui
            return true;
        }
        return false;
    }

    /**
     * Método que permite preencher os dados do utilizador da exposição
     *
     */
    private void fillInData() {
        for (Integer i = 1; i <= 4; i++) {
            this.ru.registaUtilizador(new Utilizador("Utilizador " + i.toString(), "mail" + i.toString() + "@exposicoes.pt",
                    "username" + i.toString(), "pass" + i.toString() + Boolean.TRUE));
        }
    }
}

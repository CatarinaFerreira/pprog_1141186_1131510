package model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Catarina
 */

public class MecanismoExperienciaFae implements MecanismoAtribuicao{

    private ListaAtribuicoes la;
    
    private CentroExposicoes ce;
    
    /**
     * Construtor do mecanismo com base no critério de experiência dos FAE.
     * @param ce Centro de Expsosições.
     */
    
    public MecanismoExperienciaFae(CentroExposicoes ce) {
        la = new ListaAtribuicoes();
        this.ce=ce;
    }
    
    /**
     * Acede à lista de atribuições.
     * 
     * @param e exposicao.
     * @return 
     */
    
    @Override
    public List<Atribuicao> getListaAtribuicoes(Exposicao e) {
        
        List<Candidatura> candidaturas = e.getListaCandidaturas().getListaCandidaturas();
        List<FAE> faes = e.getListaFAE().getListaFaes();
        double[] percentagensExp = getPercentagensExperiencia(faes);
        List<Atribuicao> result = new ArrayList();
        int offset=0;
        int i;
        int tempValue;
        for (i=0;i<faes.size();i++) {
            tempValue = (int)Math.round(percentagensExp[i]*candidaturas.size());
            defineAssociacoes(faes.get(i), candidaturas,offset,tempValue,result);
            offset+=tempValue;
        }
        return result;
    }
    
    /**
     * Método que regista as atribuições.
     * 
     * @param la lista atribuição.
     * @param e exposição.
     * @return 
     */

    @Override
    public boolean registerAtribuicoes(List<Atribuicao> la,Exposicao e) {
        boolean result = this.la.registerAtribuicoes(la);
        if (result)
        {
            e.setListaAtribuicoes(this.la);
            this.la=null;
        }
        return result;
    }
    
     /**
     * Define as associações.
     * 
     * @param fae FAE.
     * @param candidaturas lista candidaturas.
     * @param offset serve como contador ("marca o início do array").
     * @param maxLength comprimento total do array.
     * @param list lista de atribuições.
     */

    private void defineAssociacoes(FAE fae,List<Candidatura> candidaturas,int offset,int maxLength,List<Atribuicao> list) {
        int i=0;
        for (i=0;i<maxLength;i++) {
            Atribuicao a = la.newAtribuicao(candidaturas.get(i+offset), fae);
            if (la.validateAtribuicoes(a))
            {
                list.add(a);
            }
        }
    }
    
    /**
     * Obtém a experiência de um conjunto de FAE com base na sua participação em exposições.
     * 
     * @param faes FAE.
     * @return 
     */
    
    private double[] getPercentagensExperiencia(List<FAE> faes) {
        double[] result = new double[faes.size()];
        double sum=0;
        for (int i=0;i<faes.size();i++) {
            result[i]=ce.getRegistoExposicoes().getListaExposicoesDoFAE(faes.get(i).getUtilizador()).size();
            sum+=result[i];
        }
        for (int i=0;i<result.length;i++) {
            result[i]/=sum;
        }
        return result;
    }

    /**
     * Acede à descrição do mecanismo.
     * @return 
     */
    
    @Override
    public String getDescricao() {
        return "Experiência de FAEs em exposições";
    }
    
    /**
     * Devolve a descrição textual do mecanismo.
     * @return 
     */
    
    @Override
    public String toString() {
        return getDescricao();
    }    
}

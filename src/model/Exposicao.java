/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Catarina
 */
public class Exposicao {

    /**
     * Variável de instância que guarda o título da exposição.
     */
    private String m_sTitulo;

    /**
     * Variável de instância que guarda a descrição da exposição.
     */
    private String m_sDescritivo;

    /**
     * Variável de instância que guarda a data de início da exposição.
     */
    private Date m_DtInicio;

    /**
     * Variável de instância que guarda a data do fim da exposição.
     */
    private Date m_DtFim;

    /**
     * Variável de instância que guarda o local de realização da exposição.
     */
    private String m_sLocal;

    /**
     * Variável de instância que guarda o ID da exposição.
     */
    private int ID;

    /**
     * Variável de instância que guarda a lista de organizadores da exposição.
     */
    private final ListaOrganizadores m_lOrganizadores;

    /**
     * Variável de instância que guarda a lista dos FAE da exposição.
     */
    private final ListaFAE m_lFAEs;

    /**
     * Variável de instância que guarda a lista com as decisões sobre as
     * candidaturas.
     */
    private final List<Decisao> m_lDecisoes;

    /**
     * Lista de atribuições definidas para esta exposição.
     */
    private ListaAtribuicoes listaAtribuicoes;

    /**
     * Variável de instância que guarda a lista com as candidaturas.
     */
    private final ListaCandidaturas m_lCandidaturas;

    /**
     * Variável de atribuição que guarda, por omissão, o título da exposição por
     * omissão.
     */
    private static final String TITULO_POR_OMISSAO = "";

    /**
     * Variável de atribuição que guarda, por omissão, a descrição da exposição.
     */
    private static final String DESCRITIVO_POR_OMISSAO = "";

    /**
     * Construtor de uma exposição que receber por parâmetros o título, a
     * descrição, a data de início, a data do fim, o local, uma lista de
     * organizadores, uma lista de FAE's, uma lista de decisões e uma lista de
     * candidaturas.
     *
     * @param m_sTitulo titulo da exposição.
     * @param m_sDescritivo descrição sobre a exposição.
     * @param m_oDtInicio data do início da exposição.
     * @param m_oDtFim data do fim da exposição.
     * @param m_sLocal local de realização da exposição.
     */
    public Exposicao(String m_sTitulo, String m_sDescritivo, Date m_oDtInicio, Date m_oDtFim, String m_sLocal) {
        this.m_sTitulo = m_sTitulo;
        this.m_sDescritivo = m_sDescritivo;
        this.m_DtInicio = m_oDtInicio;
        this.m_DtFim = m_oDtFim;
        this.m_sLocal = m_sLocal;
        this.m_lOrganizadores = new ListaOrganizadores();
        this.m_lDecisoes = new ArrayList<>();
        this.m_lFAEs = new ListaFAE();
        this.m_lCandidaturas = new ListaCandidaturas();
    }

    /**
     * Construtor de uma exposição sem parâmetros.
     */
    public Exposicao() {
        m_sTitulo = TITULO_POR_OMISSAO;
        m_sDescritivo = DESCRITIVO_POR_OMISSAO;
        this.m_lOrganizadores = new ListaOrganizadores();
        this.m_lDecisoes = new ArrayList<>();
        this.m_lFAEs = new ListaFAE();
        this.m_lCandidaturas = new ListaCandidaturas();
    }

    /**
     * Acede ao título da exposição.
     *
     * @return m_sTitulo.
     */
    public String getTitulo() {
        return m_sTitulo;
    }

    /**
     * Altera o título da exposição, recebendo um novo por parâmetro.
     *
     * @param titulo novo titulo da exposição.
     */
    public void setTitulo(String titulo) {
        this.m_sTitulo = titulo;
    }

    /**
     * Acede à descrição da exposição.
     *
     * @return m_sDescritivo descrição sobre exposição.
     */
    public String getDescritivo() {
        return m_sDescritivo;
    }

    /**
     * Altera a descrição da exposição, recebendo uma nova por parâmetro.
     *
     * @param descritivo nova descrição sobre a exposição.
     */
    public void setDescritivo(String descritivo) {
        this.m_sDescritivo = descritivo;
    }

    /**
     * Acede à data de início da exposição.
     *
     * @return m_oDtInicio data de início da exposição.
     */
    public Date getDataInicio() {
        return m_DtInicio;
    }

    /**
     * Altera a data de início da exposição, recebendo uma nova por parâmetro.
     *
     * @param dtInicio nova data de início.
     */
    public void setDataInicio(Date dtInicio) {
        this.m_DtInicio = dtInicio;
    }

    /**
     * Acede à data do fim da exposição.
     *
     * @return m_oDtFim data do fim da exposição.
     */
    public Date getDataFim() {
        return m_DtFim;
    }

    /**
     * Altera a data do fim da exposição, recebendo uma nova por parâmetro
     *
     * @param dtFim nova data do fim da exposição.
     */
    public void setDataFim(Date dtFim) {
        this.m_DtFim = dtFim;
    }

    /**
     * Acede ao local de realização da exposição.
     *
     * @return m_sLocal local de realização da exposição.
     */
    public String getLocal() {
        return m_sLocal;
    }

    /**
     * Altera o local de realização da exposição, recebendo um novo por
     * parâmetro
     *
     * @param local novo local de realização da exposição.
     */
    public void setLocal(String local) {
        this.m_sLocal = local;
    }

    /**
     * Acede ao ID da exposição.
     *
     * @return ID id da exposição.
     */
    public int getId() {
        return ID;
    }

    /**
     * Altera o período de realização da exposição (data início e data fim),
     * recebendo um novo por parâmetro.
     *
     * @param dtInicio nova data de início da exposição.
     * @param dtFim nova data do fim da exposição.
     */
    public void setPeriodo(Date dtInicio, Date dtFim) {
        this.setDataInicio(dtInicio);
        this.setDataFim(dtFim);
    }

    /**
     * Acede à lista de organizadores da exposição.
     *
     * @return m_lOrganizadores lista de organizadores.
     */
    public ListaOrganizadores getListaOrganizadores() {
        return m_lOrganizadores;
    }

    /**
     * Acede à lista de FAE´s da exposição.
     *
     * @return m_lFAEs lista de FAE.
     */
    public ListaFAE getListaFAE() {
        return m_lFAEs;
    }

    /**
     * Acede à lista de candidaturas.
     *
     * @return m_lCandidaturas lista de candidaturas.
     */
    public ListaCandidaturas getListaCandidaturas() {
        return m_lCandidaturas;
    }

    /**
     * Devolve a descrição textual da exposição.
     *
     * @return descrição textual da exposição.
     */
    @Override
    public String toString() {
        String Texto;
        Texto = String.format("%s;%s;%s;%s%s;\n", this.getTitulo(), this.getDescritivo(), this.getDataInicio().toString(), this.getDataFim().toString(), this.getLocal());

        for (Organizador org : this.m_lOrganizadores.getListaOrganizadores()) {
            Texto += String.format("%s \n", org.toString());
        }

        return Texto;
    }

    /**
     * Acede à lista de candidaturas por decidir.
     *
     * @param fae FAE.
     * @return
     */
    public List<Decisao> getListaCandidaturaPorDecidir(FAE fae) {
        return null;
    }

    /**
     * Valida a exposição.
     *
     * @return
     */
    public boolean valida() {
        if (!this.m_sTitulo.equalsIgnoreCase("") || this.m_sDescritivo.equalsIgnoreCase("") || this.m_sLocal.equalsIgnoreCase("")) {
            return true;
        }
        return false;
    }

    /**
     * Método que regista a decisão tomada relativamente às candidaturas.
     *
     * @param m_decisao decisão sobre candidaturas.
     */
    public void registaDecisao(Decisao m_decisao) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Altera a lista de atribuições de candidaturas a FAE, recebendo uma nova
     * por parâmetro.
     *
     * @param la nova lista atribuições.
     */
    public void setListaAtribuicoes(ListaAtribuicoes la) {
        this.listaAtribuicoes = la;
    }

    public List<Candidatura> getListaCandidaturasFAE(FAE f) {
        List<Candidatura> listaCandidaturaFAE = new ArrayList<>();
//        for (int i = 0; i < m_listAtribuicao.getSize(); i++) {
        for (Atribuicao a : listaAtribuicoes.getListaAtribuicoes()) {

            if (a.getFae() == f) {
                Candidatura c = a.getCandidatura();
                listaCandidaturaFAE.add(c);
            }
        }
        return listaCandidaturaFAE;
    }
}

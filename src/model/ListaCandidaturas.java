package model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Catarina
 */

public class ListaCandidaturas {

    private List<Candidatura> lstCandidaturas;
    
    /**
    * Método que cria uma lista de candidaturas.
    */  
    
    public ListaCandidaturas() {
        lstCandidaturas = new ArrayList<>();
    }
    
    /**
     * Método que cria uma lista de candidaturas recebendo por parâmetro a lista de candidaturas.
     * 
     * @param lstCandidaturas lista de candidaturas.
     */
    
    public ListaCandidaturas(ListaCandidaturas lstCandidaturas){
        this.lstCandidaturas = new ArrayList<>(lstCandidaturas.getListaCandidaturas());
    }

    /**
     * Acede à lista de candidaturas a uma exposição.
     *
     * @return 
     */
    
    public List<Candidatura> getListaCandidaturas() {
        return new ArrayList(lstCandidaturas);
    }
    
    /**
     * Cria uma nova candidatura.
     *
     * @return Candidatura
     */
    
    public Candidatura novaCandidatura() {
        return new Candidatura();
    }
    
    /**
     * Permite obter o ID de uma candidatura.
     *
     * @param ID ID.
     * @return candidatura.
     */
    
    public Candidatura obterCandidatura(int ID) {
        return getListaCandidaturas().get(ID);
    }
    
    /**
     * Regista uma nova candidatura.
     * 
     * @param candidatura Candidatura.
     * @return 
     */
    
    public boolean registarCandidatura(Candidatura candidatura) {
        return lstCandidaturas.contains(candidatura)
                ? false
                : lstCandidaturas.add(candidatura);
    }
    
    /**
     * Valida uma candidatura.
     * @param candidatura Candidatura.
     * @return 
     */
    
    public boolean validaCandidatura(Candidatura candidatura) {
        if(candidatura.valida()==true){
            if(!lstCandidaturas.contains(candidatura)){
            return true;
            }
        }
        return false;
    }    
    
    /**
     * Valida os dados da candidatura inserida.
     * @param nomeComercial nome comercial do expositor.
     * @return 
     */
    
    public boolean validaDadosCandidatura(String nomeComercial) {
        for (int i = 0; i < lstCandidaturas.size(); i++) {
            if (nomeComercial.equalsIgnoreCase(lstCandidaturas.get(i).getID())) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Adiciona candidatura à lista de candidaturas.
     * 
     * @param lstCandidaturas lista de candidaturas.
     * @return 
     */
    
    public int adicionarListaCandidaturas (ListaCandidaturas lstCandidaturas){
        int totalCandidaturasAdicionadas = 0;
        for (Candidatura candidatura : lstCandidaturas.getListaCandidaturas()) {
            boolean candidaturaAdicionada = registarCandidatura(candidatura);
            if(candidaturaAdicionada){
                totalCandidaturasAdicionadas++;
            }
        }
        return totalCandidaturasAdicionadas;
    }
    
    /**
     * Permite saber o número de candidaturas existentes.
     * @return 
     */
    
    public int tamanho(){
        return this.getListaCandidaturas().size();
    }
    
    /**
     * Permite remover uma candidatura da lista de candidaturas.
     * 
     * @param candidatura Candidatura.
     * @return 
     */
    public boolean removerCandidaturas(Candidatura candidatura){
        return lstCandidaturas.remove(candidatura);
    }
}
